/**
* Template Name: Mamba - v2.0.1
* Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
!(function ($) {
  "use strict";

  // Toggle .header-scrolled class to #header when page is scrolled
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  }

  // Stick the header at top on scroll
  $("#header").sticky({
    topSpacing: 0,
    zIndex: '50'
  });

  // Smooth scroll for the navigation menu and links with .scrollto classes
  $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function (e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      e.preventDefault();
      var target = $(this.hash);
      if (target.length) {

        var scrollto = target.offset().top;
        var scrolled = 2;

        if ($('#header-sticky-wrapper').length) {
          scrollto -= $('#header-sticky-wrapper').outerHeight() - scrolled;
        }

        if ($(this).attr("href") == '#header') {
          scrollto = 0;
        }

        $('html, body').animate({
          scrollTop: scrollto
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu, .mobile-nav').length) {
          $('.nav-menu .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Mobile Navigation
  if ($('.nav-menu').length) {
    var $mobile_nav = $('.nav-menu').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function (e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
      $('.mobile-nav-overly').toggle();
    });

    $(document).on('click', '.mobile-nav .drop-down > a', function (e) {
      e.preventDefault();
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });

    $(document).click(function (e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }

  // Intro carousel
  var heroCarousel = $("#heroCarousel");
  var heroCarouselIndicators = $("#hero-carousel-indicators");
  heroCarousel.find(".carousel-inner").children(".carousel-item").each(function (index) {
    (index === 0) ?
      heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "' class='active'></li>") :
      heroCarouselIndicators.append("<li data-target='#heroCarousel' data-slide-to='" + index + "'></li>");
  });

  heroCarousel.on('slid.bs.carousel', function (e) {
    $(this).find('h2').addClass('animated fadeInDown');
    $(this).find('p').addClass('animated fadeInUp');
    $(this).find('.btn-get-started').addClass('animated fadeInUp');
  });

  // Back to top button
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  $('.back-to-top').click(function () {
    $('html, body').animate({
      scrollTop: 0
    }, 1500, 'easeInOutExpo');
    return false;
  });

  // Initiate the venobox plugin
  $(window).on('load', function () {
    $('.venobox').venobox();
  });

  // jQuery counterUp
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });

  // Porfolio isotope and filter
  $(window).on('load', function () {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item',
      layoutMode: 'fitRows'
    });

    $('#portfolio-flters li').on('click', function () {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');

      portfolioIsotope.isotope({
        filter: $(this).data('filter')
      });
    });

    // Initiate venobox (lightbox feature used in portofilo)
    $(document).ready(function () {
      $('.venobox').venobox();
    });
  });

  // Initi AOS
  AOS.init({
    duration: 1000,
    easing: "ease-in-out-back"
  });

})(jQuery);


function moveToID(id) {
  if (id != '') {
    document.getElementById(id).scrollIntoView({
      behavior: 'smooth'
    });
    document.getElementById("navbarNavAltMarkup").classList.remove("show");
  }
}

function openCloseMenu(id) {
  var element = document.getElementById("hamburger");
  element.classList.toggle('hover');
  Drop(0);
  if (id != '') {
    document.getElementById(id).scrollIntoView({
      behavior: 'smooth'
    });
  }
}

function buttonMenu(id) {
  if (id != '') {
    document.getElementById(id).scrollIntoView({
      behavior: 'smooth'
    });
  }
}


var open = false;

function Drop(n) {
  var i;
  if (open == false) {
    for (i = n; i < 5; i++) {
      Drp(i)
    }
    open = true;
    var element = document.getElementById("menu-bg");
    element.classList.add("menu-bg");
  } else if (open == true) {
    for (i = n; i < 5; i++) {
      Cls(i)
    }
    open = false;
    var element = document.getElementById("menu-bg");
    element.classList.remove("menu-bg");
  }
}

function Drp(n) {
  var elem = document.getElementsByClassName("menu-con")[n];
  var pos = -1 * window.innerHeight - n * 100;
  var id = setInterval(frame, 5);

  function frame() {
    if (pos >= -10) {
      clearInterval(id);
      elem.style.top = 0 + 'px';
    } else {
      pos += 10;
      elem.style.top = pos + 'px';
    }
  }
}

function Cls(n) {
  var elems = document.getElementsByClassName("menu-con")[n];
  var poss = 0;
  var ids = setInterval(frames, 5);

  function frames() {
    if (poss <= -1 * window.innerHeight) {
      clearInterval(ids);
      elems.style.top = -1 * window.innerHeight + 'px';
    } else {
      poss += -7 - n * 2;
      elems.style.top = poss + 'px';
    }
  }
}

const video = document.getElementById("video");
const circlePlayButton = document.getElementById("circle-play-b");

// function togglePlay() {
//   if (video.paused || video.ended) {
//     video.play();
//   } else {
//     video.pause();
//   }
// }

// circlePlayButton.addEventListener("click", togglePlay);
// video.addEventListener("playing", function () {
//   circlePlayButton.style.opacity = 0;
// });
// video.addEventListener("pause", function () {
//   circlePlayButton.style.opacity = 1;
// });

// values to keep track of the number of letters typed, which quote to use. etc. Don't change these values.
var i = 0,
  a = 0,
  isBackspacing = false,
  isParagraph = false;

// Typerwrite text content. Use a pipe to indicate the start of the second line "|".  
var textArray = [
  "Your imagination, Our innovation",
  "Intelligence of tomorrow, delivered today",
  // "Empower growth with Robotic Automation",
  "Create, augment and Sustain.",
  // "Why shouldn't you write with a broken pencil?|Because it's pointless",
];

// Speed (in milliseconds) of typing.
var speedForward = 50, //Typing Speed
  speedWait = 5000, // Wait between typing and backspacing
  speedBetweenLines = 1000, //Wait between first and second lines
  speedBackspace = 25; //Backspace Speed

//Run the loop
typeWriter("output", textArray);

function typeWriter(id, ar) {
  var element = $("#" + id),
    aString = ar[a],
    eHeader = element.children("h1"), //Header element
    eParagraph = element.children("p"); //Subheader element

  // Determine if animation should be typing or backspacing
  if (!isBackspacing) {

    // If full string hasn't yet been typed out, continue typing
    if (i < aString.length) {

      // If character about to be typed is a pipe, switch to second line and continue.
      if (aString.charAt(i) == "|") {
        isParagraph = true;
        eHeader.removeClass("cursor");
        eParagraph.addClass("cursor");
        i++;
        setTimeout(function () { typeWriter(id, ar); }, speedBetweenLines);

        // If character isn't a pipe, continue typing.
      } else {
        // Type header or subheader depending on whether pipe has been detected
        if (!isParagraph) {
          eHeader.text(eHeader.text() + aString.charAt(i));
        } else {
          eParagraph.text(eParagraph.text() + aString.charAt(i));
        }
        i++;
        setTimeout(function () { typeWriter(id, ar); }, speedForward);
      }

      // If full string has been typed, switch to backspace mode.
    } else if (i == aString.length) {

      isBackspacing = true;
      setTimeout(function () { typeWriter(id, ar); }, speedWait);

    }

    // If backspacing is enabled
  } else {

    // If either the header or the paragraph still has text, continue backspacing
    if (eHeader.text().length > 0 || eParagraph.text().length > 0) {
      eHeader.html('</br>');

      // If paragraph still has text, continue erasing, otherwise switch to the header.
      if (eParagraph.text().length > 0) {
        eParagraph.text(eParagraph.text().substring(0, eParagraph.text().length - 1));
      } else if (eHeader.text().length > 0) {
        eParagraph.removeClass("cursor");
        eHeader.addClass("cursor");
        eHeader.text(eHeader.text().substring(0, eHeader.text().length - 1));
      }
      setTimeout(function () { typeWriter(id, ar); }, speedBackspace);

      // If neither head or paragraph still has text, switch to next quote in array and start typing.
    } else {

      isBackspacing = false;
      i = 0;
      isParagraph = false;
      a = (a + 1) % ar.length; //Moves to next position in array, always looping back to 0
      setTimeout(function () { typeWriter(id, ar); }, 50);

    }
  }
}

window.onscroll = function () { myFunction() };

// Get the header
var header = document.getElementById("header");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}




let iTab = 2;


$(document).ready(function () {
  var radius = 200;
  var fields = $('.itemDot');
  var container = $('.dotCircle');
  var width = container.width();
  radius = width / 2.5;

  var height = container.height();
  // console.log(height, width);
  var angle = 0, step = (2 * Math.PI) / fields.length;
  fields.each(function () {
    var x = Math.round(width / 2 + radius * Math.cos(angle) - $(this).width() / 2);
    var y = Math.round(height / 2 + radius * Math.sin(angle) - $(this).height() / 2);
    /*if (window.console) {
      console.log($(this).text(), x, y);
    }*/

    $(this).css({
      left: x + 'px',
      top: y + 'px'
    });
    angle += step;
  });


  $('.itemDot').click(function () {

    var dataTab = $(this).data("tab");
    $('.itemDot').removeClass('active');
    $(this).addClass('active');
    $('.CirItem').removeClass('active');
    $('.CirItem' + dataTab).addClass('active');
    iTab = dataTab;

    $('.dotCircle').css({
      "transform": "rotate(" + (360 - (iTab - 1) * 90) + "deg)",
      "transition": "2s"
    });
    $('.itemDot').css({
      "transform": "rotate(" + ((iTab - 1) * 90) + "deg)",
      "transition": "1s"
    });


  });

  // setInterval(function () {
  //   var dataTab = $('.itemDot.active').data("tab");
  //   if (dataTab > 4 || iTab > 4) {
  //     dataTab = 1;
  //     iTab = 1;
  //   }
  //   $('.itemDot').removeClass('active');
  //   $('[data-tab="' + iTab + '"]').addClass('active');
  //   $('.CirItem').removeClass('active');
  //   $('.CirItem' + iTab).addClass('active');
  //   iTab++;


  //   $('.dotCircle').css({
  //     "transform": "rotate(" + (360 - (iTab - 2) * 90) + "deg)",
  //     "transition": "2s"
  //   });
  //   $('.itemDot').css({
  //     "transform": "rotate(" + ((iTab - 2) * 90) + "deg)",
  //     "transition": "1s"
  //   });

  // }, 10000);

});

$(".industries-p").click(function (e) {
  e.stopPropagation();
  $header = $(this);
  $content = $header.next();
  console.log($content);
  if ($content.is(':visible')) {
    $content.hide(500);
  } else {
    $content.show(500);
  }
});

$(".industry-head").click(function (e) {
  e.stopPropagation();
  $header = $(this);
  $content = $header.next();
  if ($content.hasClass("card-slide")) {
    $content.removeClass("card-slide");
    $content.slideUp();
  } else {
    $(".card-body").slideUp();
    $(".card-body").removeClass("card-slide");
    $content.addClass("card-slide");
    $('.card-slide').show(500);
  }
});

$(window).click(function () {
  $(".card-body").slideUp();
});


$(".tabs01 a[data-toggle]").on("click", function (e) {
  console.log('tab01');
  e.preventDefault(); // prevent navigating
  var selector = $(this).data("toggle"); // get corresponding element
  $(".tabs01-content div").hide();
  $(selector).show();
});

$(".tabs01 > a:first-child").addClass("active");
$(".tabs01 a").click(function () {
  console.log('tab012');
  var index = $(this).index();
  if ($(this).hasClass("active")) {
    $(this).removeClass("active");
  } else {
    $(".myLink01").removeClass("active");
    $(this).addClass("active");
  }
});

$(".tabs02 a[data-toggle]").on("click", function (e) {
  console.log('tab02');
  e.preventDefault(); // prevent navigating
  var selector = $(this).data("toggle"); // get corresponding element
  $(".tabs02-content div").hide();
  $(selector).show();
});

$(".tabs02 > a:first-child").addClass("active");
$(".tabs02 a").click(function () {
  console.log('tab022');
  var index = $(this).index();
  if ($(this).hasClass("active")) {
    $(this).removeClass("active");
  } else {
    $(".myLink02").removeClass("active");
    $(this).addClass("active");
  }
});