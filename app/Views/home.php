<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Astuten</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url() ?>assets/img/dark.png" rel="icon">
  <link href="<?php echo base_url() ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>assets//vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets//vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets//vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets//vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets//vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets//vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url() ?>assets//css/style.css?v=1.19" rel="stylesheet">

</head>

<body>
  <!-- ======= Top Bar ======= -->
  <!-- <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="#">gawaremitesh@gmail.com</a>
        <i class="icofont-phone"></i><a href="">+91 8087-477-803</a>
      </div>
      <div class="social-links float-right">
        <a href="#" class="#"><i class="icofont-twitter"></i></a>
        <a href="#" class="#"><i class="icofont-facebook"></i></a>
        <a href="#" class="#"><i class="icofont-instagram"></i></a>
        <a href="#" class="#"><i class="icofont-skype"></i></a>
        <a href="#" class="#"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </section> -->

  <!-- ======= Header ======= -->
  <div class="header" id="header">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand d-flex" href="<?php echo base_url('') ?>">
          <img src="<?php echo base_url() ?>assets//img/logo-light.png" width="30" height="30" class="d-inline-block align-top" alt="">
          <h3 class="logo-name">Astuten</h3>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-item nav-link" onclick="moveToID('about');">Our Story</a>
            <a class="nav-item nav-link active" onclick="moveToID('industries');">Industries</a>
            <a class="nav-item nav-link"><span class="tba"></span></a>
            <a class="nav-item nav-link" onclick="moveToID('solutions');">Solutions</a>
            <a class="nav-item nav-link">Blog</a>
            <a class="nav-item nav-link" onclick="moveToID('case-studies');">Case Studies</a>
          </div>
        </div>
      </nav>
    </div>
  </div>
  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <!-- <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
        <div class="carousel-inner" role="listbox">
          
        <div class="carousel-item active" style="background-image: url('<?php echo base_url() ?>assets//img/slide/slide-1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animated fadeInDown">Welcome to <span>Astuten</span></h2>
                <p class="animated fadeInUp">Monitoring various parameters in industry, such as temperature, leakages, equipments health using IOT and robotics.</p>
                <a href="#about" class="btn-get-started animated fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>
          
          <div class="carousel-item" style="background-image: url('<?php echo base_url() ?>assets//img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animated fadeInDown">Cloud Computing</h2>
                <p class="animated fadeInUp">Using cloud computing, AI/ML to analyse the above data and generate detailed analysis and prediction.</p>
                <a href="#about" class="btn-get-started animated fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>
          
          <div class="carousel-item" style="background-image: url('<?php echo base_url() ?>assets//img/slide/slide-4.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animated fadeInDown">Automated Guided Vehicles (AGV’s)</h2>
                <p class="animated fadeInUp">Using AGV’s to move materials from one point to another.AGVs are a valuable tool for improving efficiency and reducing costs in industrial and logistical operations</p>
                <a href="#about" class="btn-get-started animated fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </section> -->
  <!-- End Hero -->

  
  <!-- Particles -->
  <section class="hero-particles" id="hero">
    <!-- <div id="particles-js" class="particles-js"></div> -->
    <div class="particles-text d-flex">
      <div class="container m-auto">
        <!-- <h2 class="animated fadeInDown">Welcome to <br/>Astuten</h2> -->
        <!-- <p class="animated fadeInUp">The forefront of automation and IoT innovation using cloud computing, AI/ML to analyze the data and generate detailed reports and prediction.</p> -->
        
        <div class="output" id="output">
          <h1 class="cursor"></h1>
        </div>
        <div class="mt-4">
        <a onclick="buttonMenu('contact');" class="btn-get-started">Connect with Us</a>
        </div>
      </div>
    </div>
  </section>
  <!-- End Particles -->

  <main id="main">

  <!-- Video -->
  <div class="video">
    <video class="bg-video" playsinline autoplay muted loop>
      <!-- <source src="<?php echo base_url() ?>assets//video/1017009613-preview.mp4" type="video/webm"> -->
      <source src="<?php echo base_url() ?>assets//video/bg-video-2.mp4" type="video/mp4">
    </video>
  </div>
  <!-- End Video -->

  <!-- ======= About Us Section ======= -->
    <div id="about" class="spacer"></div>
    <section class="about">
      <div class="container">
        
        <div class="row no-gutters">
          <div class="col-lg-12 about-section">
            <div class=" pr-2">
              <h1>Our Story</h1>
              
            </div>
            
          </div>
          <div class="col-lg-12 d-flex flex-column justify-content-center about-content">
            <div class="section-title">
              <p>Astuten laid its foundation in February- 2019. At Astuten we are passionate about enabling industries to automate industrial processes using niche technology .  Astuten is built on three technology pillars-  innovation- designing - deploying robust and effective technology solutions in diverse industries.</p>
              <p>The company is the brainchild of  Mr. Varun Singha, an M.Tech. in Computer Science from BITS, Pilani. Varun has been a professional architect in the Cloud and Robotics ecosystem for 14 years.</p>
              <p>The team at Astuten is built on passion and our ability to astute results through lenses of   innovation and technology.  From fully automating your business through technology-based solutions to accessible and sophisticated technological spin-offs is the mission we carry at Astuten. Proficient at building automated, intuitive and secure solutions leveraging evolving technology stack of - Robotics, AI/ML, IIOT, and cloud computing. We are committed to our work, accountable to our solutions and passionate about our customer's growth.</p>
              <p>Our solutions are primarily designed for automation, inspection and safety for industries such as  power, O&G, food, cement, automobiles, heavy engineering and similar industrial  sectors.</p>
              <p>Join the metal club. <a class="cursor" onclick="moveToID('contact');">Connect with us!!</a></p>
            </div>
            <!-- <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bxs-like bx-spin-hover"></i></div>
              <h4 class="title"><a href="">Automated guided vehicles (AGV’s)</a></h4>
              <p class="description">Sometimes called self-guided vehicles or autonomous guided vehicles, automated guided vehicles (AGVs) are material handling systems or load carriers that travel autonomously throughout a warehouse, distribution center, or manufacturing facility, without an onboard operator or driver</p>
            </div>
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bxs-like bx-fade-right-hover"></i></div>
              <h4 class="title"><a href="">Steel industry,</a></h4>
              <p class="description">Steel is a versatile material that is used in a wide range of products, including construction materials, vehicles, appliances, and many other consumer good</p>
            </div> -->
          </div>
        </div>
      </div>
    </section>
    <!-- End About Us Section -->

    <!-- ======= About Lists Section ======= -->
    <div id="solutions" class="spacer"></div>


    <section class="iq-features">
      <div class="section-title">
          <h2>Solutions</h2>
      </div>
      <div class="container">
        <div class="row m-0">
          <div class="col-lg-8 col-md-12">
            <div class="holderCircle">
              <div class="round"></div>
              <div class="dotCircle">
                <span class="itemDot active itemDot1" data-tab="1">
                  <!-- <i class="fa fa-android"></i> -->
                  <img src="<?php echo base_url() ?>assets//img/robot.png" alt="" />
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot2" data-tab="2">
                  <!-- <i class="fa fa-desktop"></i> -->
                  <img src="<?php echo base_url() ?>assets//img/magnifying-glass.png" alt="" />
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot3" data-tab="3">
                  <!-- <i class="fa fa-user-secret"></i> -->
                  <img src="<?php echo base_url() ?>assets//img/iot.png" alt="" />
                  <span class="forActive"></span>
                </span>
                <span class="itemDot itemDot4" data-tab="4">
                  <!-- <i class="fa fa-hand-lizard-o"></i> -->
                  <img src="<?php echo base_url() ?>assets//img/robot-arm.png" alt="" />
                  <span class="forActive"></span>
                </span>
              </div>
              
            </div>
          </div>
          <div class="col-lg-4 col-md-12 d-flex m-auto">
            <div class="contentCircle">
                <div class="CirItem title-box active CirItem1">
                  <h2 class="title"><span>Robotic Automation</span></h2>
                  <p>Custom built/ lidar based</p>
                  <p>AGV</p>
                  <p>Line Following</p>
                  <p>IOT Robots</p>
                  <!-- <i class="fa fa-clock-o"></i> -->
                </div>
                <div class="CirItem title-box CirItem2">
                  <h2 class="title"><span>Anomaly Detection</span></h2>
                  <p>Prediction</p>
                  <p>Plant Safety</p>
                  <p>Inspection And Monitoring</p>
                  <!-- <i class="fa fa-comments"></i> -->
                </div>
                <div class="CirItem title-box CirItem3">
                  <h2 class="title"><span>Industrial IOT</span></h2>
                  <p>Industrial Security Solutions</p>
                  <!-- <i class="fa fa-user"></i> -->
                </div>
                <div class="CirItem title-box CirItem4">
                  <h2 class="title"><span>Robotic Arms</span></h2>
                  <p>TBA</p>
                  <p>Niche Tech enabled Robotic manufacturing arms</p>
                  <!-- <i class="fa fa-tags"></i> -->
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!-- <section class="about-lists">
      <div class="container pb-5">
        <h3 class="text-center pb-3">Solutions</h3>
        <div class="row no-gutters">
          <div class="col-lg-6 col-md-6 content-item">
            <span>01</span>
            <h5>Robotic Automation — Custom built/ lidar based, AGV, Line Following, IOT Robots</h5>
            <p>Robots are widely used in such industries as automobile manufacture to perform simple repetitive tasks</p>
          </div>
          <div class="col-lg-6 col-md-6 content-item">
            <span>02</span>
            <h5>Anomaly Detection — Prediction, Plant Safety, Inspection And Monitoring</h5>
          </div>
          <div class="col-lg-6 col-md-6 content-item">
            <span>03</span>
            <h5>Industrial IOT — Industrial Security Solutions</h5>
          </div>
          <div class="col-lg-6 col-md-6 content-item">
            <span>04</span>
            <h5>Robotic Arms — TBA, Niche Tech enabled Robotic manufacturing arms</h5>
          </div>
        </div>
      </div>
    </section> -->
    <!-- End About Lists Section -->

    

    <!-- ======= Counts Section ======= -->
    <!-- <section class="counts section-bg">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-lg-3 col-md-6 text-center p-3" data-aos="fade-up">
            <div class="count-box">
              <i class="icofont-simple-smile" style="color: #20b38e;"></i>
              <span data-toggle="counter-up">432</span>
              <p>Happy Clients</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center p-3" data-aos="fade-up" data-aos-delay="200">
            <div class="count-box">
              <i class="icofont-document-folder" style="color: #c042ff;"></i>
              <span data-toggle="counter-up">521</span>
              <p>Projects</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center p-3" data-aos="fade-up" data-aos-delay="400">
            <div class="count-box">
              <i class="icofont-live-support" style="color: #46d1ff;"></i>
              <span data-toggle="counter-up">1,463</span>
              <p>Hours Of Support</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center p-3" data-aos="fade-up" data-aos-delay="600">
            <div class="count-box">
              <i class="icofont-users-alt-5" style="color: #ffb459;"></i>
              <span data-toggle="counter-up">15</span>
              <p>Hard Workers</p>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- End Counts Section -->

    <!-- <section class="scroller section-bg">
      <h2 class="text-center pb-5 text-color">Our Esteemed Clients</h2>
      <div class="">
        <marquee direction="left" height="100px" class="mb-5">
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c1.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c2.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c3.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c4.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c5.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c6.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c7.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c8.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c9.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c10.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c11.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c12.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c14.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c15.svg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c16.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c17.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c18.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c19.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c20.webp" alt="" />
        </marquee>
        <marquee direction="right" height="100px">
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c10.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c11.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c12.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c14.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c15.svg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c16.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c17.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c18.jpg" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c19.png" alt="" />
          <img class="cl-img" src="<?php echo base_url() ?><?php echo base_url() ?>assets//img/clients/c20.webp" alt="" />
        </marquee>
      </div>
    </section> -->

    <!-- ======= Our Products======= ->
    <div id="portfolio" class="spacer"></div>
    <section class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">
        <div class="section-title">
          <h2>Our Products</h2>
          <p> wide range of products that are essential in various industries Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="col-lg-12">
          <div class="">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-wire">Wire Products</li>
              <li data-filter=".filter-steel">Specialty Steel Products</li>
              <li data-filter=".filter-pipe">Pipes and Tubes</li>
            </ul>
          </div>
        </div>
        <div class="portfolio-container">
          <div class="col-lg-4 col-md-6 portfolio-item filter-wire">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/wire-3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Wire</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/wire-3.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-pipe">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/pipe-1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Pipe</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/pipe-1.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-wire">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/wire-4.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Wire</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/wire-4.jpg" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-steel">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/steel-bar-1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Bar</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/steel-bar-1.jpg" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-pipe">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/pipe-2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Pipe</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/pipe-2.jpg" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-wire">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/wire-5.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Wire</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/wire-5.jpg" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-steel">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/steel-sheet.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Sheet</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/steel-sheet.jpg" data-gall="portfolioGallery" class="venobox" title="Card 1"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-steel">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/steel-screw.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel Screw</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/steel-screw.jpg" data-gall="portfolioGallery" class="venobox" title="Card 3"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-pipe">
            <div class="portfolio-wrap">
              <img src="<?php echo base_url() ?>assets//img/portfolio/pipe-3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Steel pipe</h4>
                <p>Lorem ipsum dolor sit amet.</p>
                <div class="portfolio-links">
                  <a href="<?php echo base_url() ?>assets//img/portfolio/pipe-3.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="icofont-eye"></i></a>
                  <a href="#" title="More Details"><i class="icofont-external-link"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!- End Our Portfolio Section -->

    <!-- <section class="servies pb-5 pt-5" id="courses">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="center-heading">
                        <h2 class="section-title">Our Services</h2>
                    </div>
                </div>
                <div class="offset-lg-3 col-lg-6">
                    <div class="center-text">
                        <p>Robotic services refer to the use of robots or robotic systems to perform various tasks and services.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <a href="#" class="services-small-item active">
                        <div class="icon">
                            <i class="fa fa-shopping-cart "></i>
                        </div>
                        <h2 class="services-title">Manufacturing</h2>
                        <p>Robots are extensively used in manufacturing and industrial processes to automate tasks such as assembly</p>
                        <div class="button">
                            <i class="fa fa-chevron-right"></i>
                        </div>
                    </a>
                    <div class="item-bg"></div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="#" class="services-small-item">
                        <div class="icon">
                            <i class="fa fa-bed"></i>
                        </div>
                        <h5 class="services-title">Healthcare</h5>
                        <p>Robots are extensively used in manufacturing and industrial processes to automate tasks such as assembly</p>
                        <div class="button">
                            <i class="fa fa-chevron-right"></i>
                        </div>
                    </a>
                    <div class="item-bg"></div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="#" class="services-small-item">
                        <div class="icon">
                            <i class="fa fa-bed"></i>
                        </div>
                        <h5 class="services-title">Security</h5>
                        <p>Robots are extensively used in manufacturing and industrial processes to automate tasks such as assembly</p>
                        <div class="button">
                            <i class="fa fa-chevron-right"></i>
                        </div>
                    </a>
                    <div class="item-bg"></div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <a href="#" class="services-small-item">
                        <div class="icon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <h5 class="services-title">Maintenance</h5>
                        <p>Robots are extensively used in manufacturing and industrial processes to automate tasks such as assembly</p>
                        <div class="button">
                            <i class="fa fa-chevron-right"></i>
                        </div>
                    </a>
                    <div class="item-bg"></div>
                </div>
            </div>
        </div>
    </section> -->
    
    <!-- ======= Industries Section ======= -->
    <!-- <div id="industries" class="spacer"></div> -->
    <section id="industries" class="services">
      <div class="section-title">
          <h2>Industries</h2>
      </div>
      <p class="industries-p">Our solution segments</p>
      <div class="container" style="display: none;">
        <div class="row m-0">
          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/power.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Power</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <!-- <span class="tag tag-teal">Factory</span> -->
                <p>Integrating robotics and IOT based solutions in power plants to revolutionise the industry by improving production efficiency, enhancing work safety and accurate  software prediction in compliance with environmental regulations.</p>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/oil-gas.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Oil & Gas</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <!-- <span class="tag tag-teal">Technology</span> -->
                <p>Custom solutions for the oil and gas industry to  enhance safety, efficiency, and cost-effectiveness, while reducing human exposure to hazardous environments and improving productivity. Our Robotic systems improve production efficiency, ensure consistent quality, and enhance worker safety. This integration contributes to increased productivity and cost-effectiveness.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/chemical.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Chemicals</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>Robotic systems improve production efficiency, ensure consistent quality, and enhance worker safety by handling hazardous materials and performing repetitive tasks. This integration contributes to increased productivity and cost-effectiveness in chemical manufacturing operations.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/engineering.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Heavy Engineering</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>Robotic systems enables Heavy Engineering industries to achieve higher production volumes, superior product quality, and cost optimisation via automation. Enabling automated assembly, welding and inspection processes in the automobile industry. Our Systems improve production efficiency, precision, and consistency, while reducing cycle times and minimising errors.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/automobile.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Automobile</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>Robotics has transformed the automobile industry by enabling automated assembly, welding, painting, and inspection processes. Robotic systems improve production efficiency, precision, and consistency, while reducing cycle times and minimizing errors. This integration has revolutionized the industry by enhancing productivity, quality, and safety in manufacturing operations.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/fertilizer.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Fertilizers</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>Integertating IOT systems with on ground robotics enhance efficiency while reducing labor costs and minimising wastage. This integration enables fertiliser manufacturers to achieve higher productivity, consistent product quality, and improved operational effectiveness.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/aviation.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Aviation</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>The combination of IoT and robotics in the aviation industry has revolutionised operations by enabling real-time data monitoring, predictive maintenance, and autonomous tasks.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/food.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Food & Beverage</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>The integration of robotics in the food and beverage industry has transformed operations by automating tasks such as food preparation, packaging, and sorting. Robotic systems enhance efficiency, accuracy, and consistency while maintaining strict hygiene standards. This integration improves productivity, reduces labor costs, and ensures high-quality products in the F&B sector.</p>
              </div>  
            </div>
          </div>

          <div class="col-md-4">
            <div class="card" data-aos="fade-up">
              <div class="card-header industry-head cursor">
                <img src="<?php echo base_url() ?>assets//img/solutions/healthcare.jpg" alt="rover" />
                <div class="w-100 text">
                  <h5 class="text-center">Healthcare</h5>
                </div>
              </div>
              <div class="card-body text-center">
                <p>IoT based data analytics in healthcare has revolutionised patient care by enabling remote monitoring, precise surgical procedures, and personalised healthcare solutions.</p>
              </div>  
            </div>
          </div>

        </div>
      </div>
    </section>

    <!-- Case studies -->
    <!-- <div id="case-studies" class="spacer"></div> -->
    <section id="case-studies" class="services">
      <div class="section-title">
        <h2 class="">Case Studies</h2>
      </div>
      <div class="container">
        <div class="col-md-12">
          <div class="mb-5 pb-5">
            <div class="card-header case-header">
              <h6 class="pt-2"><b>#1: Mobile robots for blowpipe inspection and monitoring</b></h6>
            </div>
            <div class="case-body">
              <div class="row w-100 m-0">
              
                <div class="tabs01">
                  <a href="#" data-toggle="#div1" class="myLink01 active">
                  Challenge
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                  <a href="#" data-toggle="#div2" class="myLink01">
                  Aim
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                  <a href="#" data-toggle="#div3" class="myLink01">
                  Solution
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                </div>
                <div class="tabs01-content">
                  <div id="div1">
                    <ul>
                      <li>Manual inspection of the blowpipes</li>
                      <li>Lack of computation as well as data analysis</li>
                      <li>Life uncertainty and human unsafety</li>
                    </ul>
                  </div>
                  <div id="div2">
                    <ul>
                      <li>To increase blowpipe lifespan and prevent accidents</li>
                      <li>To collect in-depth analytical and operational data</li>
                      <li>To conduct safe inspection and monitoring of the blowpipe</li>
                    </ul>
                  </div>
                  <div id="div3">
                    <ul>
                      <li>Mobile robot equipped with camera and sensors</li>
                      <li>Hardware: Thermal Imaging Camera, IOT device and Robotics</li>
                    </ul>
                  </div>
                </div>

                <!-- <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Challenge
                      </div>
                      <div class="back">
                        <ul>
                          <li>Manual inspection of the blowpipes</li>
                          <li>Lack of computation as well as data analysis</li>
                          <li>Life uncertainty and human unsafety</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Aim
                      </div>
                      <div class="back">
                        <ul>
                          <li>To increase blowpipe lifespan and prevent accidents</li>
                          <li>To collect in-depth analytical and operational data</li>
                          <li>To conduct safe inspection and monitoring of the blowpipe</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Solution
                      </div>
                      <div class="back">
                        <ul>
                          <li>Mobile robot equipped with camera and sensors</li>
                          <li>Hardware: Thermal Imaging Camera, IOT device and Robotics</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div> -->

              </div>  
            </div>  
          </div>

          <div class="mb-5">
            <div class="card-header case-header">
              <h6 class="pt-2"><b>#2: Robotics automation for coal dumping</b></h6>
            </div>
            <div class="case-body">
              <div class="row w-100 m-0">

                <div class="tabs02">
                  <a href="#" data-toggle="#div11" class="myLink02 active">
                  Challenge
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                  <a href="#" data-toggle="#div12" class="myLink02">
                  Aim
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                  <a href="#" data-toggle="#div13" class="myLink02">
                  Solution
                  <span class="plus-arrow">
                    <span class="line1 line"></span>
                    <span class="line2 line"></span>
                  </span>
                </a>
                </div>
                <div class="tabs02-content">
                  <div id="div11">
                    <ul>
                      <li>Coal dumping is conducted through manual intervention</li>
                      <li>Risk to human safety as well as wellness</li>
                      <li>Time requirement is higher</li>
                    </ul>
                  </div>
                  <div id="div12">
                    <ul>
                      <li>To conduct an easy deployment of coal drop hose to coal dumping yard</li>
                      <li>To ensure faster dumping and increased recycling</li>
                    </ul>
                  </div>
                  <div id="div13">
                    <ul>
                      <li>Integration of AGV based line following robotic system</li>
                      <li>Hydraulics for the outer system and load detectors</li>
                    </ul>
                  </div>
                </div>
                


                <!-- <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Challenge
                      </div>
                      <div class="back">
                        <ul>
                          <li>Coal dumping is conducted through manual intervention</li>
                          <li>Risk to human safety as well as wellness</li>
                          <li>Time requirement is higher</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Aim
                      </div>
                      <div class="back">
                        <ul>
                          <li>To conduct an easy deployment of coal drop hose to coal dumping yard</li>
                          <li>To ensure faster dumping and increased recycling</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                      <div class="front">
                        Solution
                      </div>
                      <div class="back">
                        <ul>
                          <li>Integration of AGV based line following robotic system</li>
                          <li>Hydraulics for the outer system and load detectors</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div> -->

              </div>  
            </div>  
          </div>

          <!-- <div class="card" data-aos="fade-up">
            <div class="card-header">
              <h6 class="pt-2"><b>#2: Robotics automation for coal dumping</b></h6>
            </div>
            <div class="card-body case-body">
              <div class="row w-100">
                <div class="col-md-4">
                  <h5 class="w-100 text-center"><b>Challenge</b></h5>
                  <ul>
                    <li>Coal dumping is conducted through manual intervention</li>
                    <li>Risk to human safety as well as wellness</li>
                    <li>Time requirement is higher</li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h5 class="w-100 text-center"><b>Aim</b></h5>
                  <ul>
                    <li>To conduct an easy deployment of coal drop hose to coal dumping yard</li>
                    <li>To ensure faster dumping and increased recycling</li>
                  </ul>
                </div>
                <div class="col-md-4">
                  <h5 class="w-100 text-center"><b>Solution</b></h5>
                  <ul>
                    <li>Integration of AGV based line following robotic system</li>
                    <li>Hydraulics for the outer system and load detectors</li>
                  </ul>
                </div>
              </div>  
            </div>  
          </div> -->

        </div>
      </div>


      <!-- <div class="ag-format-container container">
        <div class="ag-courses_box">
          <div class="col-md-6 pb-4">
            <div class="ag-courses_item">
              <div class="ag-courses-item_link">
                <div class="ag-courses-item_bg"></div>
                  <div class="ag-courses-item_title">
                    Security and Surveillance
                  </div>
                  <div class="ag-courses-item_date-box">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 pb-4">
            <div class="ag-courses_item">
              <div class="ag-courses-item_link">
                <div class="ag-courses-item_bg"></div>
                  <div class="ag-courses-item_title">
                    Education and Research
                  </div>
                  <div class="ag-courses-item_date-box">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 pb-4">
            <div class="ag-courses_item">
                <div class="ag-courses-item_link">
                  <div class="ag-courses-item_bg"></div>
                    <div class="ag-courses-item_title">
                    Agriculture and Farming
                    </div>
                    <div class="ag-courses-item_date-box">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                </div>
            </div>
          </div>  

          <div class="col-md-6 pb-4">  
            <div class="ag-courses_item">
              <div class="ag-courses-item_link">
                <div class="ag-courses-item_bg"></div>
                  <div class="ag-courses-item_title">
                    Delivery and Logistics
                </div>
                <div class="ag-courses-item_date-box">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
                </div>
            </div>
          </div>  -->
          
        </div>
    </section>
  

    

    <!-- ======= Frequently Asked Questions Section ======= -->
    <!-- <section id="faq" class="faq section-bg">
      <div class="container">
        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>
        <div class="row m-0 d-flex align-items-stretch">
          <div class="col-lg-6 faq-item" data-aos="fade-up">
            <h4>What is steel made of?</h4>
            <p>
              Steel is primarily made of iron, with small amounts of carbon and other elements added to give it specific properties. The exact composition of steel can vary depending on the intended use.
            </p>
          </div>
          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="100">
            <h4>What are some common applications of steel?</h4>
            <p>
              Steel is used in a wide range of applications, from construction and infrastructure to manufacturing and transportation. Some common uses include structural steel for buildings and bridges, sheet steel for automobiles and appliances, and pipe and tubing for oil and gas pipelines.
            </p>
          </div>
          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="200">
            <h4>What are some challenges facing the steel industry?</h4>
            <p>
              The steel industry faces a number of challenges, including competition from alternative materials such as aluminum and composite materials, fluctuations in demand and prices, and environmental concerns related to the production process and waste disposal.
            </p>
          </div>
          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="300">
            <h4>What is the future of the steel industry?</h4>
            <p>
              The future of the steel industry is likely to be shaped by technological advances, changing global economic conditions, and evolving environmental regulations. Some experts predict a shift toward more sustainable production methods and greater use of recycled materials.
            </p>
          </div>
          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="400">
            <h4>What are the different types of robots?</h4>
            <p>
            There are many different types of robots, including industrial robots used in manufacturing, service robots used in healthcare and hospitality, and entertainment robots used for education and entertainment. Other types of robots include military robots, agricultural robots, and household robots.
            </p>
          </div>
          <div class="col-lg-6 faq-item" data-aos="fade-up" data-aos-delay="500">
            <h4>What are some common components of a robot?</h4>
            <p>
              Robots typically include a control system, sensors, actuators, and effectors. The control system is the brain of the robot, while sensors enable it to detect and respond to its environment. Actuators allow the robot to move and manipulate objects, while effectors are the tools or devices used to perform specific tasks.
            </p>
          </div>
        </div>
      </div>
    </section> -->
    <!-- End Frequently Asked Questions Section -->

    <!-- Contact -->
    <section class="contact" id="contact">
      <div class="container">
        <div class="section-title">
          <h2>Get in Touch with Us</h2>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-12 p-2 align-items-stretch aos-init aos-animate">
            <div class="col-lg-12">
            <p class="text-color">We would love to hear from you! Please fill out the form below and we will get back to you as soon as possible.</p>
            </div>
            <form>
              <div class="row m-0">
                <div class="col-sm-6">
                  <input type="text" class="form-control" placeholder="Name">
                </div>
                
                <div class="col-sm-6">
                  <input type="email" class="form-control" placeholder="Work Email">
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" placeholder="Designation">
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" placeholder="Phone (Optional)">
                </div>
                <div class="col-sm-12">
                  <div class="select">
                    <select>
                      <option value="1">Industry</option>
                      <option value="2">Aviation</option>
                      <option value="3">Fertilisers</option>
                      <option value="3">Automobile</option>
                      <option value="3">Healthcare</option>
                      <option value="3">Manufacturing</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="p-3 my-3">
                <button class="btn btn-block" type="submit">Send Now!</button>
              </div>
            </form>
          </div>
          
          <!-- <div class="col-lg-3 p-2 pt-4 align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
            <div class="info-box mt-5">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
              <p>info@astuten.in</p>
            </div>
            <div class="info-box ">
              <i class="bx bx-phone-call"></i>
              <h3>Call Us</h3>
              <p>+91-8087-477-803<br>+91-7829-006-185</p>
            </div>
          </div> -->
        </div>
      </div>
    </section>
    <!-- End Contact Us Section -->
  
  </main>
  <!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 footer-info">
            <a class="navbar-brand d-flex" href="<?php echo base_url('') ?>">
              <img src="<?php echo base_url() ?>assets//img/logo-light.png" width="30" height="30" class="d-inline-block align-top mr-2" alt="">
              <h3 class="logo-name">Astuten</h3>
            </a>
            <p>
              <strong><a href="mailto:info@astuten.in">info@astuten.in</a></strong><br>
              <strong>Phone:</strong> +91-7829-006-185<br>
            </p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>
          <!-- <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a onclick="openCloseMenu('hero');">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a onclick="openCloseMenu('about');">About Us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a onclick="openCloseMenu('portfolio');">Products</a></li>
              <li><i class="bx bx-chevron-right"></i> <a onclick="openCloseMenu('solutions');">Solutions</a></li>
              <li><i class="bx bx-chevron-right"></i> <a onclick="openCloseMenu('contact');">Contact</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div> -->
          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Our Solutions</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> Robotic Automation</li>
              <li><i class="bx bx-chevron-right"></i> Anomaly Detection</li>
              <li><i class="bx bx-chevron-right"></i> Industrial IOT</li>
              <li><i class="bx bx-chevron-right"></i> Robotic Arms</li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Astuten</span></strong>. All Rights Reserved | Designed by <span>Empyef Technologies</span>
      </div>
    </div>
  </footer>
  <!-- End Footer -->

  <a href="#" class="back-to-top">
    <svg class="arrow up" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="5 0 50 80" xml:space="preserve">
    <polyline fill="none" stroke="#FFFFFF" stroke-width="8" stroke-linecap="round" stroke-linejoin="round" points="
0.375, 35.375 28.375, 0.375 58.67, 35.375 " />
  </svg>
  </a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>assets//js/particles.js?v=1.19"></script>
  <script src="<?php echo base_url() ?>assets//js/app.js?v=1.19"></script>

  <script src="<?php echo base_url() ?>assets//vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>assets//vendor/aos/aos.js"></script>
  

  <!-- Template Main JS File -->
  <script src="<?php echo base_url() ?>assets//js/main.js?v=1.19"></script>

</body>

</html>